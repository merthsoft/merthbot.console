﻿using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using MerthBot;
using MerthBot.Console.Models;
using Bot = MerthBot.Bot;

namespace MerthBotConsole
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            string file;
            if (args.Length == 0)
            {
                file = "config.json";
            }
            else
            {
                file = string.Join(' ', args);
            }

            Log($"Reading config file: {file}");

            if (!File.Exists(file))
                throw new FileNotFoundException("Could not find config file.", file);

            var configFile = await File.ReadAllTextAsync(file)
                ?? throw new Exception("Could not read config file.");

            var config = JsonSerializer.Deserialize<Config>(configFile, new JsonSerializerOptions
            {
                AllowTrailingCommas = true,
                PropertyNameCaseInsensitive = true,
                IgnoreReadOnlyProperties = true,
                ReadCommentHandling = JsonCommentHandling.Skip,
            }) ?? throw new Exception("Could not load config file.");

            Log("Loaded configuration.");
            var bot = new Bot(config);
            bot.OnLog += Bot_OnLog;
            Console.OutputEncoding = Encoding.Unicode;

            await bot.Initialize();

            Console.WriteLine("Press Ctrl+Q to quit...");
            while (true)
            {
                await Task.Delay(0);
                var key = Console.ReadKey();
                if (key.Modifiers.HasFlag(ConsoleModifiers.Control) && key.Key == ConsoleKey.Q)
                {
                    bot.Log("Requested to quit.");
                    break;
                }
            }

            await bot.Death();
        }

        private static void Bot_OnLog(object? sender, LogEvent e)
            => Log(e.Message, e.LogLevel);

        private static void Log(string message, LogLevel logLevel = LogLevel.Message)
        {
            switch (logLevel)
            {
                case LogLevel.Debug:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case LogLevel.Message:
                    Console.ResetColor();
                    break;
                case LogLevel.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case LogLevel.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                default:
                    break;
            }

            Console.WriteLine($"[{DateTime.Now.ToShortTimeString()}] {message}");
        }
    }
}
