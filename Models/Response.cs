﻿using MerthBot.Interfaces;
using System.Collections.Generic;

namespace MerthBot.Console.Models
{
    public class Response : IResponse
    {
        public IReadOnlyCollection<string> Inputs { get; set; } = null!;

        public string Output { get; set; } = null!;
    }
}
