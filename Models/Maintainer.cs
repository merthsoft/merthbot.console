﻿using MerthBot.Interfaces;

namespace MerthBot.Console.Models
{
    public class Maintainer : IMaintainer
    {
        public ulong GuildId { get; set; }

        public ulong ChannelId { get; set; }

        public ulong UserId { get; set; }
    }
}
