﻿using MerthBot.Interfaces;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MerthBot.Console.Models
{
    public class Config : IConfig
    {
        public string TokenEnvironmentVariable { get; set; } = null!;
        
        public string? Game { get; set; }

        public int? MessageCacheSize { get; set; }

        public Maintainer Maintainer { get; set; } = null!;
        IMaintainer IConfig.Maintainer => Maintainer;

        public string FileRoot { get; set; } = null!;

        public string? FileRootEnvironmentVariable { get; set; }

        public List<Command> Commands { get; set; } = new List<Command>();
        IReadOnlyCollection<ICommand> IConfig.Commands => Commands.ToReadOnlyCollection<ICommand>();

        public Uri EmojiMapUri { get; set; } = null!;

        [JsonExtensionData]
        public Dictionary<string, object> ExtraData { get; set; } = new Dictionary<string, object>();
        IReadOnlyDictionary<string, object> IConfig.ExtraData => ExtraData;
    }
}
