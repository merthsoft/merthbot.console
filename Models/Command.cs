﻿using MerthBot.Interfaces;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MerthBot.Console.Models
{
    public class Command : ICommand
    {
        [JsonPropertyName("command")]
        public string Name { get; set; } = "";

        public string ImageTag { get; set; } = "";

        public IReadOnlyCollection<string> Triggers { get; set; } = new List<string>();

        public List<Response> Responses { get; set; } = new List<Response>();
        IReadOnlyCollection<IResponse> ICommand.Responses => Responses.ToReadOnlyCollection<IResponse>();

        public string Action { get; set; } = "";

        public string Description { get; set; } = "";

        public IReadOnlyCollection<string> RandomResponses { get; set; } = new List<string>();

        [JsonExtensionData]
        public Dictionary<string, object> ExtraData { get; set; } = new Dictionary<string, object>();
        IReadOnlyDictionary<string, object> ICommand.ExtraData => ExtraData;
    }
}
