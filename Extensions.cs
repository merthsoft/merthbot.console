﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MerthBot.Console
{
    static class Extensions
    {
        public static IReadOnlyCollection<TOut> ToReadOnlyCollection<TOut>(this IEnumerable set)
            => new ReadOnlyCollection<TOut>(set.Cast<TOut>().ToList());
    }
}
